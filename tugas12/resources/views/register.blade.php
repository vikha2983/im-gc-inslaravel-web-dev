<!DOCTYPE html>
<html>
    <head>
        <title>Buat Account Baru</title>
    </head>
    <body>
        <header>
            <h1>Buat Account Baru</h1> 
            <p>Sign Up form</p>
        </header>
        
        <main>
            <form method="POST" action="{{ route('welcome.post') }}">
                @csrf
                <label> First Name: </label><br>
                <input type="text" name="First_Name" placeholder=""><br><br>

                <label> Last Name: </label><br>
                <input type="text" name="Last_Name" placeholder=""><br><br>

                <label>Gender </label><br>
                <input type="radio" name="gender" value="male">Male<br>
                <input type="radio" name="gender" value="female">Female<br>
                <input type="radio" name="gender" value="other">Other<br>

                <label>Nationality: </label><br>
                <select id="Nation" name="nationality">
                    <option value="Indonesia">Indonesia</option>
                    <option value="English">English</option>
                    <option value="Korean">Korean</option>
                </select> <br> <br>

                <label>Language Spoken:</label><br>
                <input type="checkbox" name="languages[]" value="Bahasa Indonesia">Bahasa Indonesia<br>
                <input type="checkbox" name="languages[]" value="English">English<br>
                <input type="checkbox" name="languages[]" value="Other">Other<br>

                <label>Bio </label><br>
                <textarea name="bio" rows="10" cols="30"></textarea>
                <br><br>
                <input type="submit">
            </form>
        </main>
    </body>
</html>
